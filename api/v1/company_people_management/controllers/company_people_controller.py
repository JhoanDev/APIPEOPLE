from api.v1.company_people_management.managers.company_people_manager import get_companies, get_company_by_id, save_company
from api.v1.company_people_management.serializers.company_people_serializer import CompanySerializer


class CompanyController():
    @staticmethod
    def get_company(id):
        return get_company_by_id(id)

    @staticmethod
    def get_groups():
        return get_companies()

    @staticmethod
    def save_company(data):
        serializer = CompanySerializer(data=data)
        if serializer.is_valid():
            save_company(serializer)
            return serializer
        return serializer
