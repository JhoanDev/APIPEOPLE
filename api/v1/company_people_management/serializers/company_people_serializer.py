from rest_framework import serializers
from apps.people_management.models import Companies


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Companies
        fields = ('url', 'id', 'name', 'description')
        read_only = ('id',)
