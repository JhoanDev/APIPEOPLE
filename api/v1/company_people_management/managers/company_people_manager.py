from api.v1.company_people_management.serializers.company_people_serializer import CompanySerializer
from apps.people_management.models import Companies
from django.http import Http404


def get_companies():
    companies = Companies.objects.all()
    serializer = CompanySerializer(companies, many=True)
    return serializer.data


def get_company_by_id(id):
    try:
        company = Companies.objects.get(pk=id)
        serializer = CompanySerializer(company, many=False)
        return serializer.data
    except Companies.DoesNotExist:
        raise Http404
    except Exception as e:
        raise


def save_company(serializer):
    try:
        serializer.save()

    except Exception as e:
        raise
