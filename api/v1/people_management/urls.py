from django.conf.urls import url
from django.views.generic import ListView, DetailView

urlpatterns = [
    url(r'^$', ListView.as_view()),
    url(r'^(?P<pk>[\w\.-]+)/$', DetailView.as_view()),
]