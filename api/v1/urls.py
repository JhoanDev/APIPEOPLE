from django.conf.urls import url, include

urlpatterns = [
    url(r'^companies/', include('api.v1.company_people_management.urls', namespace='companies_people')),
    url(r'^people/', include('api.v1.people_management.urls', namespace='people')),
]
