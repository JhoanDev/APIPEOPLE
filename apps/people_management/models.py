from django.db import models

# Create your models here.


class Companies(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = "companies"


class People(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    age = models.IntegerField(blank=True, null=True)
    company_name = models.ManyToManyField(Companies)

    class Meta:
        db_table = "people"
