from django.apps import AppConfig


class PeopleManagementConfig(AppConfig):
    name = 'apps.people_management'
